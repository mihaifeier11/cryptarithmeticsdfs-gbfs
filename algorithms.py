from helper import *


def dfs(number, equation, dictionary, list, firstElementsIndexList):
    """
    executes the dfs algorithm
    :param number:                  the number of lines to be added
    :param equation:                a list of list containing every line of the equation
    :param dictionary:              the dictionary with the letter as the key and the index for the list as the value
    :param list:                    the list given from which the values are taken
    :param firstElementsIndexList:  the list containing the indexes for the first letter from every word
    :return:
        @list - the solution
    """
    stack = []
    stack.append(list)

    while len(stack) > 0:
        while list[0] < len(list):
            newItemsList = generateNewLevel(list, firstElementsIndexList)
            stack.extend(newItemsList)
            list = stack.pop()
        if solver(number, equation, dictionary, list):
            return list

        list = stack.pop()


def gbfs(number, equation, dictionary, list, firstElementsIndexList):
    """
    executes the dfs algorithm
    :param number:                  the number of lines to be added
    :param equation:                a list of list containing every line of the equation
    :param dictionary:              the dictionary with the letter as the key and the index for the list as the value
    :param list:                    the list given from which the values are taken
    :param firstElementsIndexList:  the list containing the indexes for the first letter from every word
    :return:
        @list - the solution
    """
    stack = []
    stack.append(list)

    while len(stack) > 0:
        while list[0] < len(list):
            newItemsList = generateNewLevel(list, firstElementsIndexList)
            newItemsList.sort(key=lambda x: fitness(number, equation, dictionary, x))
            stack.extend(newItemsList)
            list = stack.pop()
        if solver(number, equation, dictionary, list):
            return list

        list = stack.pop()
