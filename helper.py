
def generateNewLevel(list, firstElements):
    """
    generates a new level from the list given
    :param list:            the list from which a new level is generated
    :param firstElements:   the index of the first letters from each word
    :return:
        @generatedList - the list containing the new level
    """
    key = list[0]
    generatedList = []
    list[0] += 1
    startingPoint = 0

    if key in firstElements:
        startingPoint = 1

    for i in range(startingPoint, 10):
        if i not in list[1:]:
            list[key] = i
            generatedList.append(list[:])

    return generatedList


def solver(number, equation, dictionary, list):
    """
    solves the equation with the given values
    :param number:      the number of lines to be added
    :param equation:    a list of list containing every line of the equation
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return:
        @True   - if the values from the list solve the equation
        @False  - otherwise
    """
    actualResult = 0
    expectedResult = 0

    for line in equation:
        value = 0
        for element in line:
            value = value * 10 + list[dictionary[element]]

        if number > 0:
            actualResult += value
            number -= 1
        else:
            expectedResult = value

    return actualResult == expectedResult


def fitness(number, equation, dictionary, list):
    """
    calculates the fitness of the list
    :param number:      the number of lines to be added
    :param equation:    a list of list containing every line of the equation
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return:
        @totalSum - the fitness of the equation with the given values
    """
    lenRow = len(min(equation, key=lambda x: len(x)))

    totalSum = 0
    for i in range(lenRow - 1, -1, -1):
        value = 0
        for j in range(number):
            value += list[dictionary[equation[j][i]]]

        totalSum += (value - list[dictionary[equation[number][i]]])

    return totalSum
