import time
from algorithms import *
import sys
from utils import *


inputFile = sys.argv[1]
outputFile = sys.argv[2]
algorithm = sys.argv[3]

number, equation, firstElementsLetterList = readInput("input.txt")
dictionary, list, firstElementsIndexList = createDictionary(equation, firstElementsLetterList)

startTime = time.time()

if algorithm == "dfs":
    result = dfs(number, equation, dictionary, list, firstElementsIndexList)
elif algorithm == "gbfs":
    result = gbfs(number, equation, dictionary, list, firstElementsIndexList)
else:
    raise Exception("Algorithm not found")

endTime = time.time()

print("Operation took {} seconds. \n".format(round(endTime - startTime, 4)))
printToFile(outputFile, dictionary, result)
printSolution(dictionary, result)
