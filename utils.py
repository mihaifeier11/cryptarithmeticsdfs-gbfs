def readInput(file):
    """
    reads the input from the file and returns the values
    :param file: the file from where the input is read
    :return:
        @number                     - the number of lines to be added
        @equation                   - a list of list containing every line of the equation
        @firstElementsLetterList    - the list containing the first letters from every word
    """
    f = open(file, "r")
    equation = []
    number = int(f.readline())
    firstElements = []

    for i in range(number + 1):
        line = f.readline()
        line = line.replace("\n", "")
        line = line.split(",")
        firstElements.append(line[0])

        equation.append(line)

    f.close()

    return number, equation, firstElements


def createDictionary(equation, firstElementsLetterList):
    """
    creates a dictionary and a set of indexes for the first letters in every word of the equation
    :param equation:                    a list of list containing every line of the equation
    :param firstElementsLetterList:     a list containing the first letters from every word
    :return:
        @dictionary             - the dictionary with the letter as the key and the index for the list as the value
        @firstElementsIndexSet  - a set containing the indexes for the first letters of every word
    """
    dictionary = {}
    firstElementsIndexList = []
    generatedList = []
    generatedList.append(1)
    count = 1

    newEquation = list(zip(*equation))

    for element in newEquation:
        for letter in element:
            if letter not in dictionary.keys():
                dictionary[letter] = count
                generatedList.append(-1)
                count += 1

    for element in equation:
        for letter in element:
            if letter not in dictionary.keys():
                dictionary[letter] = count
                generatedList.append(-1)
                count += 1

    for letter in firstElementsLetterList:
        firstElementsIndexList.append(dictionary[letter])

    generatedList[0] = 1

    return dictionary, generatedList, firstElementsIndexList


def printSolution(dictionary, list):
    """
    prints the solution to the console
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return: -
    """
    for element in dictionary:
        if isinstance(element, str):
            print(element + ": " + str(list[dictionary[element]]))


def printToFile(filename, dictionary, list):
    """
    prints the solution to the given file
    :param filename:    the name of the file
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return: -
    """
    file = open(filename, "w")

    for element in dictionary:
        if isinstance(element, str):
            file.write(element + ", " + str(list[dictionary[element]]) + "\n")

    file.close()
